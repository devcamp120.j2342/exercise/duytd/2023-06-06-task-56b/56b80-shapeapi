package com.devcamp.b80.shapeapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.b80.shapeapi.models.Circle;

@RestController
@RequestMapping("/api")
public class CircleController {
    @CrossOrigin

    @GetMapping("/circle-area")
    public double getCircleArea(@RequestParam double radius){
        Circle circle = new Circle(radius);

        return circle.getArea();
    }

    @GetMapping("/circle-perimeter")

    public double getCirclePerimeter(@RequestParam double radius){
        Circle circle = new Circle(radius);
        return circle.getPerimeter();
    }
}
