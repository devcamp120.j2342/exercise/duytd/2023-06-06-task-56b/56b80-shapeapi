package com.devcamp.b80.shapeapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.b80.shapeapi.models.Rectangle;

@RestController
@RequestMapping("/api")
public class RectangleController {
    @CrossOrigin

    @GetMapping("/rectangle-area")

    public double getRectangleArea(@RequestParam double width, @RequestParam double length){
        Rectangle rectangle = new Rectangle(width, length);
        
        return rectangle.getArea();
    }

    @GetMapping("/rectangle-perimeter")
    public double getRectanglePerimeter(@RequestParam double width, @RequestParam double length){
        Rectangle rectangle = new Rectangle(width, length);
        return rectangle.getPerimeter();
    }
}
