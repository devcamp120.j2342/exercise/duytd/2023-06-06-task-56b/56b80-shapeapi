package com.devcamp.b80.shapeapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.b80.shapeapi.models.Square;

@RestController
@RequestMapping("/api")
public class SquareController {
    @CrossOrigin

    @GetMapping("/square-area")
    public double getSquareArea(@RequestParam double side){
        Square square = new Square(side);
        return square.getArea();
    }

    @GetMapping("/square-perimeter")
    public double getSquarePerimeter(@RequestParam double side){
        Square square = new Square(side);
        return square.getPerimeter();
    }
}
