package com.devcamp.b80.shapeapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShapeApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShapeApiApplication.class, args);
	}

}
